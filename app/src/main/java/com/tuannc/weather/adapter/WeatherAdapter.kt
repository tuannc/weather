package com.tuannc.weather.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tuannc.weather.R
import com.tuannc.weather.common.Common
import com.tuannc.weather.common.Constants
import com.tuannc.weather.databinding.ItemWeatherBinding
import com.tuannc.weather.model.ListWeather
import com.tuannc.weather.model.Weather

class WeatherAdapter : RecyclerView.Adapter<WeatherViewHolder>() {

    private var weathers = ArrayList<ListWeather>()

    fun setWeatherList(weathers: ArrayList<ListWeather>) {
        this.weathers = weathers
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemWeatherBinding.inflate(inflater, parent, false)
        return WeatherViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val weather = weathers[position]
        holder.binding.tvDescription.text = holder.binding.tvDescription.context.getString(
            R.string.str_weather_description,
            Common.convertLongTime(weather.dt ?: 0L, Constants.FORMAT_DATE),
            Common.convertKelvinToCelsius(Common.averageTemperature(weather.temp)),
            weather.pressure,
            "${weather.humidity}%",
            getDescriptionWeather(weather.weather)
        )
    }

    private fun getDescriptionWeather(list: ArrayList<Weather>?): String? {
        return if (list.isNullOrEmpty() || list.size <= 0) "" else list[0].description
    }

    override fun getItemCount(): Int {
        return weathers.size
    }
}

class WeatherViewHolder(val binding: ItemWeatherBinding) : RecyclerView.ViewHolder(binding.root)