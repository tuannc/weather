package com.tuannc.weather.repository

import com.tuannc.weather.common.Constants
import com.tuannc.weather.retrofit.Service

class WeatherRepository(private val retrofitService: Service) {
    fun getWeather(province: String) = retrofitService.getWeather(
        q = province, cnt = Constants.cntNumber, appId = Constants.appID
    )
}