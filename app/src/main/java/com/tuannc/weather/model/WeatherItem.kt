package com.tuannc.weather.model

data class WeatherItem(
    val cod: String? = "",
    val message: String? = "",
    val list: ArrayList<ListWeather>? = null
)