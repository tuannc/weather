package com.tuannc.weather.model

data class ListWeather(
    val dt: Long? = 0L,
    val temp: Temp? = null,
    val pressure: Int? = 0,
    val humidity: Int? = 0,
    val weather: ArrayList<Weather>? = null
)
/*{
    "dt": 1715572800,
    "temp": {
        "day": 309.22,
        "min": 301.31,
        "max": 309.9,
        "night": 303.74,
        "eve": 306.99,
        "morn": 301.31
    },
    "feels_like": {
        "day": 313.54,
        "night": 310.74,
        "eve": 311.53,
        "morn": 305.49
     },
    "pressure": 1009,
    "humidity": 43,
    "weather": [
        {
        "id": 500,
        "main": "Rain",
        "description": "light rain",
        "icon": "10d"
        }
    ],
    "speed": 5.12,
    "deg": 156,
    "gust": 8.88,
    "clouds": 16,
    "pop": 0.92,
    "rain": 1.39
}*/
