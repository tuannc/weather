package com.tuannc.weather.model

data class Temp(
    val day: Float = 0f,
    val min: Float? = 0f,
    val max: Float? = 0f,
    val night: Float? = 0f,
    val morn: Float? = 0f
)
/*"temp": {
    "day": 309.22,
    "min": 301.31,
    "max": 309.9,
    "night": 303.74,
    "eve": 306.99,
    "morn": 301.31
    }*/
