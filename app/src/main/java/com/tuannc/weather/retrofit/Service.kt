package com.tuannc.weather.retrofit

import com.tuannc.weather.model.WeatherItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Service {
    @GET("/data/2.5/forecast/daily?")
    fun getWeather(
        @Query("q") q: String?, @Query("cnt") cnt: String?, @Query("appid") appId: String?
    ): Call<WeatherItem>
}