package com.tuannc.weather.retrofit

import com.tuannc.weather.common.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private val retrofit: Retrofit.Builder by lazy {
        Retrofit.Builder().baseUrl(Constants.urlServer)
            .addConverterFactory(GsonConverterFactory.create())
    }

    val service: Service by lazy {
        retrofit.build().create(Service::class.java)
    }
}