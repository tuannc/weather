package com.tuannc.weather.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tuannc.weather.common.Common
import com.tuannc.weather.common.Constants
import com.tuannc.weather.database.ApiCache
import com.tuannc.weather.database.RealmDatabase
import com.tuannc.weather.model.WeatherItem
import com.tuannc.weather.repository.WeatherRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherViewModel(private val repository: WeatherRepository) : ViewModel() {
    var weatherItem: MutableLiveData<WeatherItem> = MutableLiveData<WeatherItem>()
    fun getWeather(province: String) {
        val cache: ApiCache? = RealmDatabase.getInstance()?.getApiCache(province)
        if (cache != null) {
            val weather = Common.fromJson(cache.response, WeatherItem::class.java)
            if (Common.convertLongTimeNormal(
                    cache.createdTime, Constants.FORMAT_DAY
                ) != Common.convertLongTimeNormal(cache.createdTime, Constants.FORMAT_DAY)
            ) {
                RealmDatabase.getInstance()?.deleteCacheApi(province)
                callApi(province)
            } else {
                weatherItem.postValue(weather)
            }
        } else {
            callApi(province)
        }
    }

    private fun callApi(province: String) {
        val response = repository.getWeather(province)
        response.enqueue(object : Callback<WeatherItem> {
            override fun onResponse(call: Call<WeatherItem>, response: Response<WeatherItem>) {
                weatherItem.postValue(
                    WeatherItem(
                        cod = response.body()?.cod ?: Constants.ERRORCODE,
                        message = response.body()?.message ?: "",
                        list = response.body()?.list
                    )
                )
                RealmDatabase.getInstance()?.cacheApi(province,response.body() )
            }

            override fun onFailure(call: Call<WeatherItem>, t: Throwable) {
                weatherItem.postValue(WeatherItem(cod = Constants.ERRORCODE, message = ""))
            }
        })
    }
}