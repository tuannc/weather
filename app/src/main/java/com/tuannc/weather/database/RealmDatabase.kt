package com.tuannc.weather.database

import com.tuannc.weather.common.Common
import com.tuannc.weather.model.WeatherItem
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query

object RealmDatabase {
    private var realmDatabase: RealmDatabase? = null

    fun getInstance(): RealmDatabase? {
        if (realmDatabase == null) {
            realmDatabase = RealmDatabase
        }
        return realmDatabase
    }

    fun getApiCache(id: String): ApiCache? {
        val config = RealmConfiguration.create(schema = setOf(ApiCache::class))
        val mRealm: Realm = Realm.open(config)
        val querySingle = mRealm.query<ApiCache>("id = $0", id.replace(" ", "").lowercase()).first()
        val apiCache = querySingle.find()
        return apiCache
    }

    fun cacheApi(province: String, item: WeatherItem?) {
        val apiCache = ApiCache().apply {
            id = province.replace(" ", "").lowercase()
            response = Common.toJson(item)
            createdTime = System.currentTimeMillis()
        }
        val config = RealmConfiguration.create(schema = setOf(ApiCache::class))
        val mRealm: Realm = Realm.open(config)
        mRealm.writeBlocking {
            copyToRealm(apiCache)
        }
        mRealm.close()
    }

    fun deleteCacheApi(id: String) {
        val config = RealmConfiguration.create(schema = setOf(ApiCache::class))
        val mRealm: Realm = Realm.open(config)
        mRealm.writeBlocking {
            val querySingle =
                mRealm.query<ApiCache>("id = $0", id.replace(" ", "").lowercase()).first()
            delete(querySingle)
        }
        mRealm.close()
    }
}