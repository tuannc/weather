package com.tuannc.weather.database

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

class ApiCache : RealmObject {
    @PrimaryKey
    var id: String? = null
    var response: String? = null
    var createdTime: Long = 0L
}