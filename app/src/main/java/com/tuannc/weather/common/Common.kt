package com.tuannc.weather.common

import com.google.gson.Gson
import com.tuannc.weather.model.Temp
import java.text.SimpleDateFormat
import java.util.Date

object Common {
    val gson: Gson get() = Gson()

    fun <T> fromJson(json: String?, type: Class<T>?): T {
        return gson.fromJson(json, type)
    }

    fun toJson(any: Any?): String {
        return gson.toJson(any)
    }

    fun convertLongTime(dt: Long, format: String): String {
        val timestamp = dt * 1000L
        val format = SimpleDateFormat(format)
        return format.format(Date(timestamp))
    }

    fun convertLongTimeNormal(timestamp: Long, format: String): String {
        val format = SimpleDateFormat(format)
        return format.format(Date(timestamp))
    }

    fun convertKelvinToCelsius(kelvin: Float): Int {
        return (kelvin - 273.15f).toInt()
    }

    fun averageTemperature(temp: Temp?): Float {
        if (temp == null) return 0f
        return (temp.day + temp.min!! + temp.max!! + temp.night!! + temp.morn!!) / 5
    }
}

