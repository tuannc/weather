package com.tuannc.weather.common

object Constants {
    const val urlServer = "https://api.openweathermap.org"
    const val appID = "60c6fbeb4b93ac653c492ba806fc346d"
    const val cntNumber = "7"
    const val ERRORCODE = "999"
    const val FORMAT_DATE = "EEE, dd MMM yyyy"
    const val FORMAT_DAY = "dd"
}