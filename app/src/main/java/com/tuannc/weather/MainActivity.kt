package com.tuannc.weather

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.tuannc.weather.adapter.WeatherAdapter
import com.tuannc.weather.common.Constants
import com.tuannc.weather.databinding.ActivityMainBinding
import com.tuannc.weather.model.ListWeather
import com.tuannc.weather.repository.WeatherRepository
import com.tuannc.weather.retrofit.RetrofitClient
import com.tuannc.weather.viewmodel.ViewModelFactory
import com.tuannc.weather.viewmodel.WeatherViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: WeatherViewModel
    private lateinit var binding: ActivityMainBinding
    private var listWeather = ArrayList<ListWeather>()
    private val adapter = WeatherAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(
            this, ViewModelFactory(WeatherRepository(RetrofitClient.service))
        )[WeatherViewModel::class.java]

        binding.rvWeathers.adapter = adapter

        binding.etProvince.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                search()
                return@OnEditorActionListener true
            }
            false
        })

        binding.btnGetWeather.setOnClickListener {
            search()
        }

        viewModel.weatherItem.observe(this) {
            if (it?.cod != null) {
                binding.tvError.visibility = when (it.cod) {
                    Constants.ERRORCODE -> View.VISIBLE
                    else -> if (it.list.isNullOrEmpty()) View.VISIBLE else View.GONE
                }

                if (it.cod != Constants.ERRORCODE && !it.list.isNullOrEmpty()) {
                    populateListWeather(it.list)
                }
            }
        }
    }

    private fun search() {
        val province = binding.etProvince.text.toString()
        clearDataRc()
        if (province.length > 3) {
            viewModel.getWeather(province)
            hideKeyboard()
        }
    }

    private fun populateListWeather(list: ArrayList<ListWeather>) {
        listWeather.addAll(list.map {
            ListWeather(
                dt = it.dt,
                temp = it.temp,
                pressure = it.pressure,
                humidity = it.humidity,
                weather = it.weather
            )
        })
        adapter.setWeatherList(listWeather)
    }

    private fun clearDataRc() {
        listWeather.clear()
        adapter.setWeatherList(listWeather)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}